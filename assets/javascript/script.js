"use strict";

document.querySelector(".navbar-nav").addEventListener("click", function (e) {
  e.preventDefault();
  let targetElement = e.target;
  if (targetElement.classList.contains("nav-link")) {
    let id = e.target.getAttribute("href");
    document.querySelector(id).scrollIntoView({ behavior: "smooth" });
  }
});

document.querySelector(".btn-primary").addEventListener("click", function (e) {
  e.preventDefault();
  let id = e.target.getAttribute("href");
  document.querySelector(id).scrollIntoView({
    behavior: "smooth",
  });
});

// Fade over animation
const nav = document.querySelector(".navbar");

const handleHover = function (e) {
  if (e.target.classList.contains("nav-link")) {
    const link = e.target;
    const siblings = link.closest(".navbar").querySelectorAll(".nav-link");

    siblings.forEach((el) => {
      if (el !== link) el.style.opacity = this;
    });
  }
};

// Passing "argument" into handler
nav.addEventListener("mouseover", handleHover.bind(0.5));
nav.addEventListener("mouseout", handleHover.bind(1));
